import * as z from "https://deno.land/x/zod@v3.11.6/mod.ts";
import { configSchema } from "./config.ts";
import { buildHttpApplication } from "./http.ts";

export const app = buildHttpApplication(configSchema, async (env) => {
  return {};
});
