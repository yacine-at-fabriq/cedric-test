import { configSchema } from "./config.ts";
import { config as loadEnvironmentVariables } from "https://deno.land/x/dotenv@v3.1.0/mod.ts";

const parseEnvResult = configSchema.parse(loadEnvironmentVariables());
if (!parseEnvResult.success) {
  throw parseEnvResult.error;
}

export const env = parseEnvResult.data;
