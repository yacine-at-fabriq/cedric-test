import { HttpApplication } from "./http.ts";
import { app } from "./app.ts";
import { env } from "./deno_env.ts";

interface DenoServer<Env extends Record<string, unknown>> {
  listen(port: number, env: Env): Promise<void>;
}

export function buildDenoServer<Context, Env extends Record<string, unknown>>(
  app: HttpApplication<Context, Env>,
): DenoServer<Env> {
  const { handleRequest } = app;

  return {
    async listen(port, env) {
      const server = Deno.listen({ port });
      for await (const conn of server) {
        const httpConn = Deno.serveHttp(conn);
        for await (const requestEvent of httpConn) {
          requestEvent.respondWith(
            handleRequest(requestEvent.request, env),
          );
        }
      }
    },
  };
}

const PORT = 3000;
console.info(`Server running on http://localhost:${PORT} 🚀`);
const server = buildDenoServer(app);
server.listen(PORT, env);
