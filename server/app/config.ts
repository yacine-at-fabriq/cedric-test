import {
  boolean,
  defineConfigSchema,
  enumeration,
  integer,
  json,
  string,
} from "./config_engine.ts";

export const configSchema = defineConfigSchema({});
