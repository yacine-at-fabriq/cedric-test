import * as z from "https://deno.land/x/zod@v3.11.6/mod.ts";
import { Node } from "https://deno.land/x/router@v2.0.1/mod.ts";
import { ConfigSchema } from "./config_engine.ts";

// TODO: Have the SearchParams be a shape instead of a full schema, and properly
// type it when it is missing in the handler.

// TODO: Review the top-level API to make it compatible with Cloudflare Workers.

interface Endpoint<
  Responses extends Record<number, unknown>,
  RequestBody,
  SearchParams,
  PathParams,
  Context,
  Env extends Record<string, unknown>,
> {
  bodySchema: z.Schema<RequestBody>;
  searchParamsSchema: z.Schema<SearchParams>;
  responseBuilder: ResponseBuilder<Responses>;
  handleRequest: RequestHandler<
    Responses,
    RequestBody,
    SearchParams,
    PathParams,
    Context,
    Env
  >;
  handleInvalidRequest: InvalidRequestHandler<Responses>;
}

type InferPathParam<T extends string, M extends string> = T extends
  `:${infer O}` ? M | O : M;
type InferPathParamUnion<P extends string> = P extends `${infer A}/${infer B}`
  ? InferPathParam<A, InferPathParamUnion<B>>
  : InferPathParam<P, never>;
type InferPathParams<P extends string> = Record<
  InferPathParamUnion<P>,
  string
>;

type AnyEndpoint = Endpoint<any, any, any, any, any, any>;

export type RequestHandler<
  Responses extends Record<number, unknown>,
  RequestBody,
  SearchParams,
  PathParams,
  Context,
  Env extends Record<string, unknown>,
> = (
  requestData: RequestData<RequestBody, SearchParams, PathParams>,
  responseBuilder: ResponseBuilder<Responses>,
  context: Context,
) => Promise<Response>;

export type InvalidRequestHandler<
  Responses extends Record<number, unknown>,
> = (
  validationIssues: RequestValidationIssues,
  responseBuilder: ResponseBuilder<Responses>,
) => Promise<Response>;

interface RequestValidationIssues {
  searchParamsIssues?: z.ZodIssue[];
  bodyIssues?: z.ZodIssue[];
}

export type InvalidResponseHandler = (
  validationIssues: ResponseValidationIssues,
  responseBuilder: ResponseBuilder<any>,
) => Promise<Response>;

interface ResponseValidationIssues {
  bodyIssues: z.ZodIssue[];
}

export type ResponseBuilder<Responses extends Record<number, unknown>> =
  & {
    status<Status extends keyof Responses>(
      status: Status,
    ): Responses[Status] extends undefined ? {
      build(
        body?: Responses[Status],
        headers?: Record<string, string>,
      ): Response;
    }
      : {
        build(
          body: Responses[Status],
          headers?: Record<string, string>,
        ): Response;
      };
  }
  & (200 extends keyof Responses ? {
    build(body: Responses[200], headers?: Record<string, string>): Response;
  }
    : Record<never, never>);

interface EndpointDefinition<
  ResponsesSchemas extends Record<number, z.Schema<any>>,
  RequestBody,
  SearchParams,
> {
  responses: ResponsesSchemas;
  body?: z.Schema<RequestBody>;
  params?: z.Schema<SearchParams>;
}

interface NoBodyEndpointDefinition<
  ResponsesSchemas extends Record<number, z.Schema<any>>,
  SearchParams,
> {
  responses: ResponsesSchemas;
  params?: z.Schema<SearchParams>;
}

type ResponsesFromResponsesSchemas<
  ResponsesSchemas extends Record<number, z.Schema<any>>,
> = {
  [status in keyof ResponsesSchemas & number]: z.infer<
    ResponsesSchemas[status]
  >;
};

type FillResponsesSchemas<
  ResponsesSchemas extends Record<number, z.Schema<any>>,
> = 400 extends keyof ResponsesSchemas ? ResponsesSchemas
  : (ResponsesSchemas & { 400: typeof InvalidRequestSchema });

export const InvalidRequestSchema = z.object({
  error: z.literal("invalid_request"),
  error_description: z.string().optional(),
  search_params_issues: z.array(z.any()).optional(),
  body_issues: z.array(z.any()).optional(),
});

export interface HttpApplication<Context, Env extends Record<string, unknown>> {
  envSchema: ConfigSchema<Env>;
  get<
    Path extends string,
    ResponsesSchemas extends Record<number, z.Schema<any>>,
    SearchParams,
  >(
    path: Path,
    definition: NoBodyEndpointDefinition<
      ResponsesSchemas,
      SearchParams
    >,
    handleRequest: RequestHandler<
      ResponsesFromResponsesSchemas<FillResponsesSchemas<ResponsesSchemas>>,
      undefined,
      SearchParams,
      InferPathParams<Path>,
      Context,
      Env
    >,
  ): void;
  post<
    Path extends string,
    ResponsesSchemas extends Record<number, z.Schema<any>>,
    RequestBody,
    SearchParams,
  >(
    path: Path,
    definition: EndpointDefinition<
      ResponsesSchemas,
      RequestBody,
      SearchParams
    >,
    handleRequest: RequestHandler<
      ResponsesFromResponsesSchemas<FillResponsesSchemas<ResponsesSchemas>>,
      RequestBody,
      SearchParams,
      InferPathParams<Path>,
      Context,
      Env
    >,
  ): void;
  handleRequest(
    request: Request,
    env: Env,
  ): Promise<Response>;
}

type HttpMethod = "get" | "post" | "put" | "patch" | "delete" | "options";

type Router = {
  [method in HttpMethod]: Node<AnyEndpoint>;
};

export function buildHttpApplication<
  Context,
  Env extends Record<string, unknown>,
>(
  envSchema: ConfigSchema<Env>,
  buildContext: (env: Env) => Promise<Context>,
): HttpApplication<Context, Env> {
  const router: Router = {
    get: new Node(),
    post: new Node(),
    put: new Node(),
    patch: new Node(),
    delete: new Node(),
    options: new Node(),
  };

  function addRoute<
    Path extends string,
    ResponsesSchemas extends Record<number, z.Schema<any>>,
    RequestBody,
    SearchParams,
  >(
    method: HttpMethod,
    path: Path,
    definition: EndpointDefinition<
      ResponsesSchemas,
      RequestBody,
      SearchParams
    >,
    handleRequest: RequestHandler<
      ResponsesFromResponsesSchemas<FillResponsesSchemas<ResponsesSchemas>>,
      RequestBody,
      SearchParams,
      InferPathParams<Path>,
      Context,
      Env
    >,
    handleInvalidRequest: InvalidRequestHandler<
      ResponsesFromResponsesSchemas<FillResponsesSchemas<ResponsesSchemas>>
    > = defaultHandleInvalidRequest as InvalidRequestHandler<
      ResponsesFromResponsesSchemas<FillResponsesSchemas<ResponsesSchemas>>
    >,
    handleInvalidResponse: InvalidResponseHandler =
      defaultHandleInvalidResponse,
  ): void {
    let { responses } = definition;
    if (!(400 in responses)) {
      responses = { ...responses, 400: InvalidRequestSchema };
    }

    const responseBuilder = {
      status(status: number) {
        const responseSchema = responses[status];
        return {
          build(body: unknown, headers: Record<string, string>) {
            const headersSet = new Headers({
              ...headers,
              "content-type": "application/json",
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Headers": "*",
            });
            if (!responseSchema) {
              return new Response(null, { headers: headersSet, status: 500 });
            }
            const bodyValidation = responseSchema.safeParse(body);
            if (!bodyValidation.success) {
              return handleInvalidResponse({
                bodyIssues: bodyValidation.error.issues,
              }, responseBuilder);
            }
            return new Response(
              bodyValidation.data === undefined
                ? null
                : JSON.stringify(bodyValidation.data, null, 2),
              { headers: headersSet, status },
            );
          },
        };
      },
    } as ResponseBuilder<any>;

    if (200 in responses) {
      responseBuilder.build = responseBuilder.status(200).build;
    }

    router[method].add(path, {
      bodySchema: definition.body ?? z.undefined(),
      searchParamsSchema: definition.params ?? z.object({}),
      responseBuilder: responseBuilder,
      handleRequest,
      handleInvalidRequest,
    });
  }

  return {
    envSchema,
    get(path, definition, handleRequest) {
      addRoute("get", path, definition, handleRequest);
    },
    post(path, definition, handleRequest) {
      addRoute("post", path, definition, handleRequest);
    },
    async handleRequest(request, env) {
      const context = await buildContext(env);
      return handleRequest(router, request, context);
    },
  };
}

async function defaultHandleInvalidRequest(
  issues: RequestValidationIssues,
  res: ResponseBuilder<any>,
): Promise<Response> {
  const { searchParamsIssues, bodyIssues } = issues;
  let errorDescription = "Unknown issue.";
  if (!searchParamsIssues) {
    if (!bodyIssues) {
      errorDescription = "Invalid body and search parameters.";
    } else {
      errorDescription = "Invalid search parameters.";
    }
  } else if (!bodyIssues) {
    errorDescription = "Invalid body.";
  }
  return res.status(400).build({
    error: "invalid_request",
    error_description: errorDescription,
    search_params_issues: searchParamsIssues,
    body_issues: bodyIssues,
  });
}

async function defaultHandleInvalidResponse(
  issues: ResponseValidationIssues,
  res: ResponseBuilder<any>,
): Promise<Response> {
  return res.status(500).build({
    error: "invalid_response",
    body_issues: issues.bodyIssues,
  });
}

async function handleRequest<Context>(
  router: Router,
  request: Request,
  context: Context,
): Promise<Response> {
  const url = new URL(request.url);
  const method = request.method.toLowerCase();
  const node = router[method as HttpMethod];
  const [endpoint, pathParamsMap] = node.find(url.pathname);
  const pathParams = Object.fromEntries(pathParamsMap);
  if (method === "options") {
    return new Response(null, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "*",
      },
      status: 200,
    });
  }
  if (!endpoint) {
    return new Response(null, { status: 404 });
  }

  const {
    bodySchema,
    searchParamsSchema,
    handleRequest: handle,
    responseBuilder,
    handleInvalidRequest,
  } = endpoint;

  let requestBody: unknown;
  if (!request.body) {
    requestBody = undefined;
  } else {
    try {
      requestBody = await request.json();
    } catch (error: unknown) {
      if (error instanceof SyntaxError) {
        return responseBuilder.status(400).build({
          error: "invalid_request",
          error_description: error.message,
        });
      }
      throw error;
    }
  }
  const rawSearchParams = Object.fromEntries(url.searchParams.entries());
  const searchParamsValidation = searchParamsSchema.safeParse(rawSearchParams);
  const bodyValidation = bodySchema.safeParse(requestBody);
  if (!searchParamsValidation.success) {
    const bodyIssues = bodyValidation.success
      ? []
      : bodyValidation.error.issues;
    return handleInvalidRequest({
      searchParamsIssues: searchParamsValidation.error.issues,
      bodyIssues,
    }, responseBuilder);
  }
  if (!bodyValidation.success) {
    return handleInvalidRequest({
      searchParamsIssues: [],
      bodyIssues: bodyValidation.error.issues,
    }, responseBuilder);
  }

  const response = await handle(
    {
      request,
      pathParams,
      searchParams: searchParamsValidation.data,
      body: bodyValidation.data,
    },
    responseBuilder,
    context,
  );

  return response;
}

export interface RequestData<Body, SearchParams, PathParams> {
  readonly request: Request;
  readonly pathParams: PathParams;
  readonly searchParams: SearchParams;
  readonly body: Body;
}
