import * as z from "https://deno.land/x/zod@v3.11.6/mod.ts";

/**
 * This is a barebone declarative config engine based on Zod.
 * The main reason we are not using Zod directly is the need to parse the
 * variables, which are necessarily string.
 * Will be fleshed out in the future so we can check the validity of the config
 * at deploy time.
 */

// TODO: Default values, serialization of schema, docs generation

export interface ConfigSchema<Shape extends Record<string, unknown>> {
  parse(input: unknown): ParseResult<Shape>;
}

export type inferType<Schema extends ConfigSchema<Record<string, unknown>>> = {
  [key in keyof Schema]: Schema[key] extends ConfigParameter<infer T> ? T
    : never;
};

export function defineConfigSchema<
  ConfigDefinition extends Record<string, ConfigParameter<any>>,
>(definition: ConfigDefinition): ConfigSchema<
  {
    [key in keyof ConfigDefinition]: ConfigDefinition[key] extends
      ConfigParameter<infer T> ? T : never;
  }
> {
  const zodSchema = z.object(
    Object.fromEntries(
      Object.entries(definition).map(([key, value]) => {
        return [key, value.encoder];
      }),
    ),
  );
  return {
    parse(input: unknown) {
      return zodSchema.safeParse(input) as ParseResult<
        {
          [key in keyof ConfigDefinition]: ConfigDefinition[key] extends
            ConfigParameter<infer T> ? T : never;
        }
      >;
    },
  };
}

export function string(description: string): ConfigParameter<string> {
  return new ConfigParameter(description, z.string());
}

export function integer(description: string): ConfigParameter<number> {
  return new ConfigParameter(
    description,
    z.string().regex(/^\d+$/).transform((value) => parseInt(value, 10)),
  );
}

export function boolean(description: string): ConfigParameter<boolean> {
  return new ConfigParameter(
    description,
    z.string().regex(/^[01]$/).transform((value) => value === "1"),
  );
}

export function enumeration<U extends string, T extends [U, ...U[]]>(
  description: string,
  values: T,
): ConfigParameter<T[number]> {
  return new ConfigParameter(description, z.enum<U, T>(values));
}

export function json<T>(
  description: string,
  schema: z.Schema<T>,
): ConfigParameter<T> {
  return new ConfigParameter(
    description,
    // FIXME: Types of preprocess don't match very well.
    z.preprocess<Encoder<T>>(
      (value) => JSON.parse((value || null) as string),
      schema as any,
    ),
  );
}

type Encoder<T> = z.Schema<T, z.ZodTypeDef, string>;

class ConfigParameter<T> {
  description: string;
  encoder: Encoder<T>;
  _secret: boolean;

  constructor(description: string, encoder: Encoder<T>) {
    this.description = description;
    this.encoder = encoder;
    this._secret = false;
  }

  secret(): this {
    const parameter = Object.assign(
      Object.create(ConfigParameter.prototype),
      this,
    );
    parameter._secret = true;
    return parameter;
  }
}

type ParseResult<Shape extends Record<string, unknown>> =
  | { success: true; data: Shape }
  | { success: false; error: z.ZodError<{ [key in keyof Shape]: string }> };
